import {
  html, fixture, assert, fixtureCleanup,
} from '@open-wc/testing';
import '../cells-data-table.js';

suite('CellsDataTable', () => {
  let el;

  const columnsConfig = [
    {
      title: 'Código',
      dataField: 'customerId',
      columnRender: value => value,
    },
    {
      title: 'Nombre',
      dataField: 'first_name',
    },
  ];

  const data = [
    {
      customerId: '8089',
      first_name: 'Olvan',
    },
  ];

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-data-table></cells-data-table>`);
    await el.updateComplete;
    el.columnsConfig = columnsConfig;
    el.items = data;
    el.cls = 'dark';
    el.compact = 'compact';
  });

  test('Instantiating component', () => {
    const element = el.shadowRoot.querySelector('.wrapper');
    assert.isNotNull(element);
  });

  test('Test function configDataTable modePagination memory', () => {
    el.modePagination = 'memory';
    el.configDataTable();
  });

  test('Test function configDataTable modePagination remote', () => {
    el.modePagination = 'remote';
    el.configDataTable();
  });

  test('Test function activeCheckeds', () => {
    el.shadowRoot.querySelector('div').classList.add('row-item');
    el.activeCheckeds();
  });

  test('Test function activeCheckeds checkBoxSelection', () => {
    el.checkBoxSelection = true;
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('row-item');
    el.activeCheckeds();
  });

  test('Test function changeChk isMain checked', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('checkbox-item');
    const evt = {
      stopPropagation() {},
      target: {
        checked: true,
      },
    };
    el.changeChk(evt, true);
  });

  test('Test function changeChk isMain non checked', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('checkbox-item');
    const evt = {
      stopPropagation() {},
      target: {
        checked: false,
      },
    };
    el.changeChk(evt, true);
  });

  test('Test function changeChk checked', () => {
    const evt = {
      stopPropagation() {},
      target: {
        parentNode: {
          parentNode: {
            classList: {
              add() {},
            },
            dataset: {
              item: '{}',
            },
          },
        },
        checked: true,
      },
    };
    el.changeChk(evt, false);
  });

  test('Test function changeChk non checked', () => {
    const evt = {
      stopPropagation() {},
      target: {
        parentNode: {
          parentNode: {
            classList: {
              remove() {},
            },
            dataset: {
              item: '{}',
            },
          },
        },
        checked: false,
      },
    };
    el.changeChk(evt, false);
  });

  test('Test function verifyCheckeds', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('active');
    el.checkBoxSelection = true;
    el.verifyCheckeds();
  });

  test('Test function clearCheckBox', () => {
    el.checkBoxSelection = true;
    el.clearCheckBox();
  });

  test('Test function activeAll', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('row-item');
    el.activeAll();
  });

  test('Test function removeAllActives', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('active');
    el.removeAllActives();
  });

  test('Test function selectedRow non row', () => {
    const div = el.shadowRoot.querySelector('div');
    const evt = {
      target: div,
    };
    el.selectedRow(null, null, null, evt);
  });

  test('Test function selectedRow', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('cell');
    div.id = 'col-checkbox-1';
    el.checkBoxSelection = true;
    const evt = {
      target: div,
    };
    el.selectedRow({}, 'col-checkbox-1', 1, evt);
  });

  test('Test function selectedRow active', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('cell');
    div.classList.add('active');
    div.id = 'col-checkbox-1';
    el.checkBoxSelection = true;
    const evt = {
      target: div,
    };
    el.selectedRow({}, 'col-checkbox-1', 1, evt);
  });

  test('Test function navigationPage(+)', () => {
    el.checkBoxSelection = true;
    el.navigationPage('+');
  });

  test('Test function navigationPage(+)', () => {
    el.checkBoxSelection = true;
    el.navigationPage('-');
  });

  test('Test function navigationPage(+) memory', () => {
    el.checkBoxSelection = true;
    el.modePagination = 'memory';
    el.navigationPage('-');
  });

  test('Test function navigationPage(+) remote', () => {
    el.checkBoxSelection = true;
    el.modePagination = 'remote';
    el.navigationPage('-');
  });

  test('Test function changePage memory', () => {
    el.checkBoxSelection = true;
    el.modePagination = 'memory';
    const select = {
      value: '1',
    };
    el.changePage(select);
  });

  test('Test function changePage remote', () => {
    el.checkBoxSelection = true;
    el.modePagination = 'remote';
    const select = {
      value: '1',
    };
    el.changePage(select);
  });

  test('Test function sortTable', () => {
    el.checkBoxSelection = true;
    const columnConfig = {
      title: 'Código',
      sort: true,
      dataField: 'customerId',
      columnRender: value => value,
    };
    el.modePagination = 'memory';
    el.shadowRoot
      .querySelector('#header-col-0')
      .querySelector('cells-icon')
      .classList.remove('hide');
    el.sortTable(columnConfig, 'header-col-0', 0);
  });

  test('Test function sortTable down', () => {
    el.checkBoxSelection = true;
    const columnConfig = {
      title: 'Código',
      sort: true,
      dataField: 'customerId',
      columnRender: value => value,
    };
    el.shadowRoot
      .querySelector('#header-col-0')
      .querySelector('cells-icon').icon = 'coronita:down';
    el.shadowRoot
      .querySelector('#header-col-0')
      .querySelector('cells-icon')
      .classList.remove('hide');
    el.sortTable(columnConfig, 'header-col-0', 0);
  });

  test('Test function sortTable up', () => {
    el.checkBoxSelection = true;
    const columnConfig = {
      title: 'Código',
      sort: true,
      dataField: 'customerId',
      columnRender: value => value,
    };
    el.shadowRoot
      .querySelector('#header-col-0')
      .querySelector('cells-icon').icon = 'coronita:up';
    el.shadowRoot
      .querySelector('#header-col-0')
      .querySelector('cells-icon')
      .classList.remove('hide');
    el.sortTable(columnConfig, 'header-col-0', 0);
  });

  test('Test function execSort desc', () => {
    el.items = [
      {
        customerId: '8089',
        first_name: 'Olvan',
      },
      {
        customerId: '8089',
        first_name: 'Olvan',
      },
      {
        customerId: '8022',
        first_name: 'Olvan',
      },
      {
        customerId: '8033',
        first_name: 'Olvan',
      },
    ];
    el.execSort('customerId', 'desc');
  });

  test('Test function execSort asc', () => {
    el.items = [
      {
        customerId: '8089',
        first_name: 'Olvan',
      },
      {
        customerId: '8089',
        first_name: 'Olvan',
      },
      {
        customerId: '8022',
        first_name: 'Olvan',
      },
      {
        customerId: '8033',
        first_name: 'Olvan',
      },
    ];
    el.execSort('customerId', 'asc');
  });

  test('Test function execSort number asc', () => {
    el.items = [
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 3,
        first_name: 'Olvan',
      },
      {
        customerId: 5,
        first_name: 'Olvan',
      },
    ];
    el.execSort('customerId', 'asc');
  });

  test('Test function execSort number desc', () => {
    el.items = [
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 3,
        first_name: 'Olvan',
      },
      {
        customerId: 5,
        first_name: 'Olvan',
      },
    ];
    el.execSort('customerId', 'desc');
  });

  test('Test buttons click', () => {
    el.items = [
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 3,
        first_name: 'Olvan',
      },
      {
        customerId: 5,
        first_name: 'Olvan',
      },
    ];
    el.modePagination = 'memory';
    el.shadowRoot.querySelectorAll('button').forEach(b => b.click());
  });

  test('Test Inputs click', () => {
    el.items = [
      {
        customerId: 1,
        first_name: 'Olvan',
        checked: true,
        styleRowCustom: 'color:black;',
      },
      {
        customerId: 1,
        first_name: 'Olvan',
      },
      {
        customerId: 3,
        first_name: 'Olvan',
      },
      {
        customerId: 5,
        first_name: 'Olvan',
      },
    ];
    el.modePagination = 'memory';
    el.shadowRoot.querySelectorAll('input').forEach(b => b.click());
  });

  test('Test function getSelections', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('active');
    div.setAttribute('data-item', '{}');
    assert.isNotNull(el.getSelections());
  });

  test('Test function verifyActivesInitLoad', () => {
    const div = el.shadowRoot.querySelector('div');
    div.classList.add('row-item');
    div.classList.add('active');
    div.setAttribute('data-item', '{}');
    el.checkBoxSelection = true;
    el.verifyActivesInitLoad();
  });
});
